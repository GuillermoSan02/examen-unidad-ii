const bodyParser = require("body-parser")
const express = require("express")
const router = express.Router()

router.get("/",(req,res)=>{
    const params ={
        contrato: req.body.contrato,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        estudios: req.body.estudios,
        pago: req.body.pago,
        dias: req.body.dias
    }
    res.render('formulario.html', params);
})

router.post('/', (req,res)=>{
    const params ={
        contrato: req.body.contrato,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        estudios: req.body.estudios,
        pago: req.body.pago,
        dias: req.body.dias
    }
    res.render("formulario.html", params);
})

router.post('/mostrar', (req,res)=>{
    const params ={
        contrato: req.body.contrato,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        estudios: req.body.estudios,
        pago: req.body.pago,
        dias: req.body.dias
    }
    res.render("mostrar.html", params);
})

router.post('/mostrar', (req,res)=>{
    const params ={
        contrato: req.body.contrato,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        estudios: req.body.estudios,
        pago: req.body.pago,
        dias: req.body.dias
    }
    res.render("mostrar.html", params);
})



module.exports=router;